/*
@author: Oloruntobi Ayilara
Student ID: 15409543
Module Code:CSY1018
Year: 1;
Date: 24/04/2016
*/
var human,
    computer,
    betMeter,
    isGameOver = false,
    betTotal = 0;
    
//oop template from typescriptlang.org
var Player = (function () {
    
    //constructor
    function Player(name, handId, cardCounterId) {
        this.name = name;
        this.score = 0;
        this.stick = false;
        this.cash = 100;
        this.handId = handId;
        this.cardCounterId = cardCounterId;
    }
    
    //method
    Player.prototype.hit = function (callback) {
        if (isGameOver){
            return;
        }
        var val = 0;
        var card = Deck.getCard();
        
        if (card.name == 'A' && this.score + 11 <= 21){
            val = 11;
        } else {
            val = card.val;
        }
        console.log(card);
        this.score += val;
        
        console.log(this.name + ' | total:' + this.score);
        
        UI.addCard(card, this.handId, function () {
            if (callback!=undefined){
                 callback();
            }
        });
        
        UI.setCardCount(this.cardCounterId,this.score);
    };
    
     
    Player.prototype.won = function () {
        return this.score == 21;
    };
    
    Player.prototype.busted = function () {
        return this.score > 21;
    };
    
    Player.prototype.getName = function () {
        return this.name;
    };
    
    Player.prototype.reset = function () {
        this.score = 0;
        this.stick = false;
    };
    
    Player.prototype.canBet = function (amount) {
        return amount <= this.cash;
    };
    
    Player.prototype.deductCash = function (amount) {
        this.cash -= amount;
    };
    
    Player.prototype.addCash = function (amount) {
        this.cash += amount;
    };
    
    Player.prototype.getBet = function (amount) {
        console.log('cash was '+this.cash+'| I deducted '+amount);
        this.deductCash(amount);
        
        return amount;
    };
    
    Player.prototype.getCash = function () {
        return this.cash;
    };
    
    
    return Player;
} ());


var Deck = (function () {
    
    var suits = ['Clubs', 'Diamonds', 'Hearts', 'Spades'];
    var deck = [];
    
    //constructor
    function Deck() {
    }
    
    //static method
    Deck.generateCards = function () {
        for (var i = 0, j = suits.length; i < j; i++){
            for (var m = 1; m <= 13; m++){
                
                var card = {};
                card.suit = suits[i];
                
                if (m==1){
                    card.name = 'A';
                    card.val = 1;
                } else if(m==11) {
                    card.name = 'Q';
                    card.val = 10;
                } else if (m==12){
                    card.name = 'K';
                    card.val = 10;
                } else if (m==13){
                    card.name = 'J';
                    card.val = 10;
                } else {
                    card.name = '' + m;
                    card.val = m;
                }
                    
                deck.push(card);
            }
        }
    };
    
    Deck.getCard = function() {
        
        var randomIndex = getRandomNumber(deck.length);
        var card = deck.splice(randomIndex, 1)[0];

        if (Deck.getLength()==0){
            Deck.generateCards();
        }
        return card;
    };
    
    Deck.getLength = function () {
        return deck.length;
    };
    
    Deck.getCardImageUrl = function (card) {
        return 'images/cards/card' + card.suit + card.name + '.png';
    };
    
    Deck.getCardBackImageUrl = function () {
        var colors = ['blue', 'green', 'red'];
        
        return 'images/cards/cardBack_' + colors[getRandomNumber(3)] + (getRandomNumber(5)+1) + '.png';
    };
    
    return Deck;
} ());

var UI = (function() {

    

    function UI() {

    }

    UI.init = function() {
        var betBtn = document.getElementById('bet-btn');
        var closeBetDialogBtn = document.getElementById('close-bet-dialog');
        var newGameBtn = document.getElementById('new-game-btn');

        betBtn.addEventListener('click', UI.openBetDialog);
        closeBetDialogBtn.addEventListener('click', UI.closeBetDialog);
        newGameBtn.addEventListener('click',UI.newGame);
    };

    UI.newGame = function () {
        
        var newGameDialog = document.getElementById('new-game-dialog');
        newGameDialog.style.display = 'none';
        UI.reset();
        UI.disableAllActionBtn();
        UI.enableDealBtn();
        
        if (human.getCash() <= computer.getCash()){
            betMeter.setMax(human.getCash());
        } else {
            betMeter.setMax(computer.getCash());
        }
        
    };

    UI.openNewGameDialog = function() {
        var newGameDialog = document.getElementById('new-game-dialog');
        newGameDialog.style.display = 'block';
    };    

    UI.addCard = function(card, handId, callback) {
        var dealerDiv = document.getElementById('dealer');
        var handDiv = document.getElementById(handId);
        var cardDiv = UI.createCardElement(Deck.getCardImageUrl(card));

        //needed for getting position relative to window
        var handRect = handDiv.getBoundingClientRect();
        var dealerRect = dealerDiv.getBoundingClientRect();

        cardDiv.style.top = -(handDiv.offsetTop - dealerDiv.offsetTop) + 'px';
        cardDiv.style.left = -(handRect.left - dealerRect.left) + 'px';
        handDiv.appendChild(cardDiv);
        console.log("hLeft" + handRect.left);
        console.log("dLeft" + dealerRect.left);

        setTimeout(function() {
            UI.refreshDealer();
            cardDiv.classList.add('card-flipped');
            
            setTimeout(function() {
                cardDiv.style.removeProperty('top');
                cardDiv.style.removeProperty('left');
      
                setTimeout(function() {
                    callback();
                }, 700);
                
            }, 1000);
            
        }, 10);
    };

    UI.createCardElement = function(front_image) {
        var dealerDiv = document.getElementById('dealer');
        var cardDiv = document.createElement('div');
        cardDiv.className = 'card';
        var cardFront = document.createElement('div');
        cardFront.className = 'card-front';
        cardFront.style.backgroundImage = 'url(\'' + front_image + '\')';
        var cardBack = document.createElement('div');
        cardBack.className = 'card-back';
        cardBack.style.backgroundImage = dealerDiv.getElementsByClassName('card-back')[0].style.backgroundImage;

        cardDiv.appendChild(cardFront);
        cardDiv.appendChild(cardBack);

        return cardDiv;
    }

    UI.setCardCount = function(elemId, val) {
        var element = document.getElementById(elemId);
        element.innerText = val;
    };

    UI.reset = function() {
        var handDivs = document.getElementsByClassName('hand');

        for (var i = 0; i < handDivs.length; i++) {
            var element = handDivs[i];
            element.innerHTML = "";
        }
        
        UI.clearHandStatus();
        UI.showComputerStick(false);

        var cardCounters = document.getElementsByClassName('card-counter');
        for (var i = 0; i < cardCounters.length; i++) {
            var element = cardCounters[i];
            element.innerText = 0;
        }
    };

    UI.refreshDealer = function() {

        var dealerDiv = document.getElementById('dealer');
        var cardBack = dealerDiv.getElementsByClassName('card-back')[0];
        cardBack.style.backgroundImage = 'url(\'' + Deck.getCardBackImageUrl() + '\')';
    };

    UI.setOnDealListener = function(callback) {
        var dealBtn = document.getElementById('deal-btn');
        dealBtn.addEventListener('click', callback);
    };

    UI.setOnHitListener = function(callback) {
        var hitBtn = document.getElementById('hit-btn');
        hitBtn.addEventListener('click', callback);
    };

    UI.setOnStickListener = function(callback) {
        var stickBtn = document.getElementById('stick-btn');
        stickBtn.addEventListener('click', callback);
    };

    UI.setTurn = function(side) {
        //side : a or b;

        var turnDiv = document.getElementById('turn-arrow');

        if (side == 'a') {
            turnDiv.className = 'turn-a';
            
        } else if (side == 'b') {
            turnDiv.className = 'turn-b';
        } else {
            turnDiv.className = '';
        }

    };

    UI.clearHandStatus = function() {
        var statusDivs = document.getElementsByClassName('hand-status');

        for (var i = 0; i < statusDivs.length;i++) {
            var statusDiv = statusDivs[i];
                statusDiv.style.display = 'none';
        }
    };    

    UI.setHandStatus = function(status, side) {
        var sideDiv = document.getElementById('side-' + side);
        var greenDiv = sideDiv.getElementsByClassName('status-green')[0];
        var redDiv = sideDiv.getElementsByClassName('status-red')[0];
        var blueDiv = sideDiv.getElementsByClassName('status-blue')[0];


        if (status=='win'){
            greenDiv.style.display = 'block';
        }else if(status=='bust'){
            redDiv.style.display = 'block';
            redDiv.innerText = 'Bust!';
        }else if(status=='lost'){
            redDiv.style.display = 'block';
            redDiv.innerText = 'Lost!';
        }else if(status=='draw'){
            blueDiv.style.display = 'block';
            blueDiv.innerText = 'Push!';
        }

    };

    UI.showComputerStick = function(isStick) {
        var computerStickDiv = document.getElementById('computer-stick');
        if (isStick){
            computerStickDiv.style.display = 'block';
        } else {
            computerStickDiv.style.display = 'none';
        }
    };

    UI.setComputerCash = function(cash){
        var sideDiv = document.getElementById('side-a');
        var scoreCounter = sideDiv.getElementsByClassName('score-counter')[0];
        scoreCounter.getElementsByTagName('span')[0].innerText = cash;
    };

    UI.setHumanCash = function(cash){
        var sideDiv = document.getElementById('side-b');
        var scoreCounter = sideDiv.getElementsByClassName('score-counter')[0];
        scoreCounter.getElementsByTagName('span')[0].innerText = cash;
    };

    UI.updateBetValue = function(betVal) {
        var betDiv = document.getElementById('bet-amount');
        betDiv.innerText = betVal;
    };

    UI.closeBetDialog = function() {
        var betDialog = document.getElementById('bet-dialog');
        betDialog.style.display = 'none';
    };

    UI.openBetDialog = function() {
        var betDialog = document.getElementById('bet-dialog');
        betDialog.style.display = 'block';
    };

    UI.enableDealBtn = function(yes = true) {
        var dealBtn = document.getElementById('deal-btn');

        if (yes){
            dealBtn.removeAttribute('disabled');
            UI.enableBetButton(true);
        } else {
            dealBtn.setAttribute('disabled', true);
            UI.enableBetButton(false);
        }
    };

    UI.enableHitBtn = function(yes = true) {
        var hitBtn = document.getElementById('hit-btn');

        if (yes){
            hitBtn.removeAttribute('disabled');
        } else {
            hitBtn.setAttribute('disabled', true);
        }
    };

    UI.enableStickBtn = function(yes = true) {
        var stickBtn = document.getElementById('stick-btn');
        
        if (yes){
            stickBtn.removeAttribute('disabled');
        } else {
            stickBtn.setAttribute('disabled', true);
        }
    };

    UI.enableBetButton = function(yes = true) {
        var betBtn = document.getElementById('bet-btn');
        if (yes){
            betBtn.removeAttribute('disabled');
        } else {
            betBtn.setAttribute('disabled', true);
        }
    };

    UI.disableAllActionBtn = function() {
        UI.enableDealBtn(false);
        UI.enableHitBtn(false);
        UI.enableStickBtn(false);
    };    
    
    
    
    return UI;
    
})();

var UINumberStepper = (function () { 
    
    function UINumberStepper(elementId) {
        this.elementId = elementId;
        this.element = document.getElementById(elementId);
        this.val = 0;
        this.min = 0;
        this.max = 20;
        this.onChange;
        
        var plusBtn = this.element.getElementsByClassName('number-stepper-plus')[0];
        var minusBtn = this.element.getElementsByClassName('number-stepper-minus')[0];
        var inputField = this.element.getElementsByTagName('input')[0];
        this.val = parseInt(inputField.value);
        
        var that = this;
        plusBtn.addEventListener('click', function (e) {
            that.onPlusClicked(e);
        });
        minusBtn.addEventListener('click', function (e) {
            that.onMinusClicked(e);
        });
    }
    
    UINumberStepper.prototype.getValue = function () {
        var inputField = this.element.getElementsByTagName('input')[0];
        this.val = parseInt(inputField.value);
        return this.val;
    };
    
    UINumberStepper.prototype.setValue = function (newVal) {
        var inputField = this.element.getElementsByTagName('input')[0];
        this.val = newVal;
        inputField.value = newVal;

        if (this.onChange!=undefined){
            this.onChange.apply(this,[newVal]);
        }
    };
    
    UINumberStepper.prototype.onPlusClicked = function () {
        var currentVal = this.getValue();
        currentVal += 10;
        if (currentVal<=this.max){
            this.setValue(currentVal);
        } else {
            this.setValue(this.max);
        }
    };
    
    UINumberStepper.prototype.onMinusClicked = function () {
        var currentVal = this.getValue();
        currentVal -= 10;
        currentVal = (currentVal < 0) ? 0 : currentVal;
        this.setValue(currentVal);
    };
    
    UINumberStepper.prototype.setOnChangeListener = function(callback){
        this.onChange = callback;
    }

    UINumberStepper.prototype.setMax = function(maxVal) {
        this.max = maxVal;
        if (this.getValue() > maxVal){
            this.setValue(maxVal);
        }
    };    
    
    
    return UINumberStepper;
})();



function getRandomNumber(max) {
    max = (max == undefined) ? 0 : max;
    return (Math.floor(Math.random() * max));
}




function onGameOver() {
    console.log('GameOver');
    console.log('      ___You___|_Computer__');
    console.log('Cash :    '+human.getCash()+'  | '+computer.getCash());
    isGameOver = true;

    UI.disableAllActionBtn();
    UI.enableDealBtn();
    UI.setTurn();
    UI.openNewGameDialog();

    UI.setHumanCash(human.getCash());
    UI.setComputerCash(computer.getCash());    
    
}

function youWin() {
    human.addCash(computer.getBet(betTotal));
    var gameResultMessage = document.getElementById('game-result-message');
    gameResultMessage.innerText = "You Win!";
    onGameOver();
}

function youLose() {
    computer.addCash(human.getBet(betTotal));
    var gameResultMessage = document.getElementById('game-result-message');
    gameResultMessage.innerText = "You Lose!";
    onGameOver();
}

function youDraw() {
    var gameResultMessage = document.getElementById('game-result-message');
    gameResultMessage.innerText = "It's a Draw!";
    onGameOver();
}

function winOrLose() {
   
    if (human.won()) {
        //alert(human.name + 'Won!');
        
        UI.setHandStatus('lost', 'a');
        UI.setHandStatus('win', 'b');
        youWin();
    } else if (human.busted()) {
        
        //alert(human.name + ' are busted!');

        UI.setHandStatus('win', 'a');
        UI.setHandStatus('bust', 'b');

        youLose();
    } else if (computer.won()) {
        //alert(computer.name + 'Won!');
        UI.setHandStatus('win', 'a');
        UI.setHandStatus('lost', 'b');
        youLose();
    } else if (computer.busted()) {
        
        //alert(computer.name + ' is busted!');
        UI.setHandStatus('bust', 'a');
        UI.setHandStatus('win', 'b');
        youWin();
    }

}

function onBothStuck() {
    if (human.score > computer.score){
        //alert(human.name+' Win!');
        UI.setHandStatus('win', 'b');
        UI.setHandStatus('lost', 'a');
        youWin();
    } else if (computer.score == human.score){
        //draw
        //alert('Draw!');
        UI.setHandStatus('draw', 'a');
        UI.setHandStatus('draw', 'b');
        youDraw();
    } else {
        
        //alert(computer.name+' Win!');
        UI.setHandStatus('win', 'a');
        UI.setHandStatus('lost', 'b');
        youLose();
    }
}


function onHumanHit() {
    if (!human.stick) {
        UI.disableAllActionBtn();
        human.hit(function() {
            winOrLose();

            if (!computer.stick){
                computerTurn();
            } else {
                yourTurn();
            }
        });
    }
}

function onComputerHit() {
    if (!computer.stick){
        computer.hit(function() {
            winOrLose();

            if (!human.stick){
                yourTurn();
            } else {
                computerTurn();
            }
        });
        
    }
}

function onHumanStick() {
    human.stick = true;
    UI.disableAllActionBtn();
    if (human.stick && computer.stick){
        onBothStuck();
    } else {
        computerTurn();
    }    
}

function onComputerStick() {
    computer.stick = true;
    UI.showComputerStick(true);
    if (human.stick && computer.stick){
        onBothStuck();
    } else {
        yourTurn();
    }
}

function computerTurn() {
    if (isGameOver){
        return;
    }
    UI.setTurn('a');
    computerIntelligence();
}

function computerIntelligence() {
    setTimeout(function () {
        
        //here computer can choose to hit or stick
        if (computer.score >= 12 && computer.score >= human.score) {
            //stick
            onComputerStick();
        } else if (computer.score >= human.score && human.stick) {
            //stick
            onComputerStick();
        } else {
            //hit
            onComputerHit();
        }
        
    }, 3000);
}

function yourTurn() { 
    if (isGameOver){
        return;
    }
    if (human.stick) {
        computerTurn();
        return;
    }
    UI.enableHitBtn();
    UI.enableStickBtn();
    UI.setTurn('b');
    console.log('-+| your turn |+-');
}

function onBetChange(betVal) {
    UI.updateBetValue(betVal);    
}

function initPlayers() {
    human = new Player('You','hand-player','card-counter-player');
    computer = new Player('Computer','hand-computer','card-counter-computer');
    betMeter = new UINumberStepper('bet-meter');
    
    UI.setOnDealListener(dealCards);
    UI.setOnHitListener(onHumanHit);
    UI.setOnStickListener(onHumanStick);

    betMeter.setOnChangeListener(onBetChange);
}

function dealCards() {
    isGameOver = false;
    UI.enableDealBtn(false);
    
    human.reset();
    computer.reset();
    betTotal = betMeter.getValue();

    human.hit(function() {

        computer.hit(function() {
            human.hit(function() {
                computer.hit(function() {
                    winOrLose();
                    UI.setTurn('b');
                    UI.enableHitBtn();
                    UI.enableStickBtn();
                });
            });
        });

    });
    
}

function pageLoaded() {
    Deck.generateCards();
    UI.init();
    initPlayers();
}

document.addEventListener('DOMContentLoaded', pageLoaded);